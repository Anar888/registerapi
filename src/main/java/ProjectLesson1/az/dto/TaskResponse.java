package ProjectLesson1.az.dto;

import ProjectLesson1.az.enums.StatusType;
import ProjectLesson1.az.model.AppUser;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TaskResponse {
    Long id;

    String name;
    LocalDateTime deadline;
    String description;
    StatusType status;
    List<AppUser> users = new ArrayList<>();
}
