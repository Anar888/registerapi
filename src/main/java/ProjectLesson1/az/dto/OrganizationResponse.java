package ProjectLesson1.az.dto;

import ProjectLesson1.az.model.Task;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrganizationResponse {
    Long id;

    String name;
    List<Task> tasks = new ArrayList<>();
}
