package ProjectLesson1.az.dto;

import ProjectLesson1.az.enums.SearchOperation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SearchCriteria {
    private String key; //age
    private Object value; //18
    private SearchOperation operation; //GREATER_THAN
}
