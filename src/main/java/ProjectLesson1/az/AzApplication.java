package ProjectLesson1.az;

import ProjectLesson1.az.model.*;
import ProjectLesson1.az.repository.*;
import ProjectLesson1.az.service.AccountService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.TreeSet;

import static ProjectLesson1.az.enums.StatusType.ACTIVE;

@SpringBootApplication
@RequiredArgsConstructor
public class AzApplication implements CommandLineRunner {
private final OrganizationRepository organizationRepository;
private final TaskRepository taskRepository;
private  final AppUserRepository appUserRepository;
	private  final PhoneNumberRepository phoneNumberRepository;
	private  final AccountRepository accountRepository;
	private  final AccountService accountService;
	private final EntityManagerFactory emf;

	private final UserRepository userRepository;
	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	private final AuthorityRepository authorityRepository;
	public static void main(String[] args) {
		SpringApplication.run(AzApplication.class, args);
	}

	@Override
//	@Transactional
	public void run(String... args) throws Exception {
//      Authority authority=Authority.builder().authority("ADMIN").build();
//
//	  User user=User.builder().authorities(List.of(authority)).email("tunar@mail.ru").username("admin")
//			  .password(bCryptPasswordEncoder.encode("12345")).build();
//		authorityRepository.save(authority);
//		userRepository.save(user);


//
//		int a=accountService.getPrime(1001);
//		System.out.println(a);


//		EntityManager em= emf.createEntityManager();
//		em.getTransaction().begin();
//		Account account=em.find(Account.class,1l);
//		account.setName("Tural");
//		em.flush();
//		em.detach(account);
//		em.merge(account);
//
//		account.setName("Kamran");
//		em.getTransaction().commit();
//		em.close();






//		SessionFactory sf=emf.unwrap(SessionFactory.class);
//		Session session=sf.openSession();
//		session.beginTransaction();
//		Account account=session.find(Account.class,1l);
//		account.setName("Zaur");
//		session.flush();
//		session.detach(account);
//		account.setName("Rufet");
//		session.flush();
//		session.merge(account);
//
//		session.getTransaction().commit();
//		session.close();



//		Account account=new Account();
//		account.setName("Aqil");
//		accountRepository.save(account);
//		accountService.assignphonetoacc(account.getId(),List.of(1l,2l));


//		Account a= accountRepository.findById(1l).get();
//		Account b=accountRepository.findById(2l).get();
//		PhoneNumber phoneNumber1=PhoneNumber.builder().phone("2304973").account(a).build();
//		PhoneNumber phoneNumber2=PhoneNumber.builder().phone("34565432").account(a).build();
//		a.getPhoneNumberList().add(phoneNumber1);
//		a.getPhoneNumberList().add(phoneNumber2);
//		phoneNumberRepository.save(phoneNumber1);
//		phoneNumberRepository.save(phoneNumber2);
//
//		accountRepository.save(a);
//		accountRepository.save(b);
//		LocalDateTime localDateTime =
//				LocalDateTime.of(2023, Month.APRIL, 28, 14, 33, 48, 000000);
//		Task task=Task
//				.builder().name("Task2").description("desc2").deadline(localDateTime).status(ACTIVE).build();
//		AppUser user1=AppUser.builder().name("user1").
//				email("mjsjdj@mail.ru").surname("surname1").
//				password("1234").build();
//
//		task.getUsers().add(user1);
//appUserRepository.save(user1);
//taskRepository.save(task);

//		System.out.println(taskRepository.findAllByAppUserId(1l));
	}
}
