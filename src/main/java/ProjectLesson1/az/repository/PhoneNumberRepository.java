package ProjectLesson1.az.repository;

import ProjectLesson1.az.model.PhoneNumber;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneNumberRepository extends JpaRepository<PhoneNumber,Long> {
}
