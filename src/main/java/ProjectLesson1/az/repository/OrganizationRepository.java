package ProjectLesson1.az.repository;

import ProjectLesson1.az.model.AppUser;
import ProjectLesson1.az.model.Organization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface OrganizationRepository extends JpaRepository<Organization,Long> {

    @Query(value = "select * from organization where id=:id",nativeQuery = true)
    Organization getOrgById(Long id);
}
