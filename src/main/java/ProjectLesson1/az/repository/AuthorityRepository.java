package ProjectLesson1.az.repository;

import ProjectLesson1.az.model.Authority;
import ProjectLesson1.az.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AuthorityRepository extends JpaRepository<Authority,Long> {



}
