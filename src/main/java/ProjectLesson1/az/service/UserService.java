package ProjectLesson1.az.service;

import ProjectLesson1.az.dto.UserRegisterDto;
import ProjectLesson1.az.model.Authority;
import ProjectLesson1.az.model.User;
import ProjectLesson1.az.repository.AuthorityRepository;
import ProjectLesson1.az.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final AuthorityRepository authorityRepository;

    public ResponseEntity<String> register(User user) {
        if (userRepository.findByUsername(user.getUsername()) != null) {
            return ResponseEntity.badRequest().body("Username already exists.");
        }
        if (user.getUsername() == null || user.getPassword() == null) {
            return ResponseEntity.badRequest().body("Username and password are required");
        }


        User newuser = new User();
        newuser.setUsername(user.getUsername());
        newuser.setPassword(user.getPassword());
        newuser.setEmail(user.getEmail());
        Authority authority=authorityRepository.findById(1l).get();
        newuser.setAuthorities(List.of(authority));
        userRepository.save(newuser);

        return ResponseEntity.ok("User registered successfully");
    }


}
