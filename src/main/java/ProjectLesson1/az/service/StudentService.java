package ProjectLesson1.az.service;

import ProjectLesson1.az.dto.SearchCriteria;
import ProjectLesson1.az.dto.StudentSpecification;
import ProjectLesson1.az.model.Student;
import ProjectLesson1.az.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentService {

    private final StudentRepository studentRepository;

    public List<Student> getAllByCriteria(List<SearchCriteria> dto) {
        StudentSpecification studentSpecification = new StudentSpecification();
        dto.forEach(searchCriteria -> studentSpecification.add(searchCriteria));
        return studentRepository.findAll(studentSpecification);
    }

}