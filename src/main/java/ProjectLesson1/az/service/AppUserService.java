package ProjectLesson1.az.service;

import ProjectLesson1.az.dto.AppUserRequest;
import ProjectLesson1.az.dto.AppUserResponse;
import ProjectLesson1.az.model.AppUser;
import ProjectLesson1.az.repository.AppUserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class AppUserService {
    private final AppUserRepository appUserRepository;
    private final ModelMapper modelMapper;
    public AppUserResponse create(AppUserRequest request) {
        AppUser user = modelMapper.map(request, AppUser.class);
        user = appUserRepository.save(user);
        return modelMapper.map(user, AppUserResponse.class);

    }

    public void update(Long userId, AppUserRequest request) {
        AppUser user = appUserRepository.findById(userId)
                .orElseThrow(() -> new RuntimeException(String.format("AppUser with id %s not found", userId)));
        user.setName(request.getName());
        user.setEmail(request.getEmail());
        user.setPassword(request.getPassword());
        user.setSurname(request.getSurname());
        appUserRepository.save(user);
    }

    public void delete(Long userId) {

        appUserRepository.deleteById(userId);
    }

    @Transactional
    public AppUserResponse get(Long userId) {
        AppUser user = appUserRepository.findById(userId)
                .orElseThrow(() -> new RuntimeException(String.format("AppUser with id %s not found", userId)));
        return modelMapper.map(user,AppUserResponse.class);
    }
}
