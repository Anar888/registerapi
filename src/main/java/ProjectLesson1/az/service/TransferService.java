package ProjectLesson1.az.service;

import ProjectLesson1.az.model.Account;
import ProjectLesson1.az.repository.AccountRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TransferService {
    private final AccountRepository accountRepository;

    @Transactional
    @SneakyThrows
    public Account update1() {
        Account account=accountRepository.findById(1l).get();
        account.setBalance(account.getBalance()+40);
        Thread.sleep(10000);

        accountRepository.save(account);
        return account;
    }
@Transactional
@SneakyThrows
    public Account read() {
         Account account=accountRepository.findById(1l).get();
         return account;
    }

    @Transactional
    @SneakyThrows
    public Account update2() {
        Account account=accountRepository.findById(1l).get();
        account.setBalance(account.getBalance()+40);

        accountRepository.save(account);
        return account;
    }

    public List<Account> findall() {
        return accountRepository.findAll();
    }
}
