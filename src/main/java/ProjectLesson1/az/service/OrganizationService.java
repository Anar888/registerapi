package ProjectLesson1.az.service;

import ProjectLesson1.az.dto.OrganizationRequest;
import ProjectLesson1.az.dto.OrganizationResponse;
import ProjectLesson1.az.model.Organization;
import ProjectLesson1.az.repository.OrganizationRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrganizationService {
    private final OrganizationRepository organizationRepository;
    private final ModelMapper modelMapper;
    public OrganizationResponse create(OrganizationRequest request) {
        Organization organization=modelMapper.map(request,Organization.class);
        organization=organizationRepository.save(organization);
        return modelMapper.map(organization,OrganizationResponse.class);
    }


    public OrganizationResponse get(Long organizationId) {
        Organization organization = organizationRepository.findById(organizationId)
                .orElseThrow(() -> new RuntimeException
                        (String.format("Organization with id %s not found", organizationId)));
        return modelMapper.map(organization,OrganizationResponse.class);
    }

    public void delete(Long organizationId) {
        organizationRepository.deleteById(organizationId);
    }

    public void update(Long organizationId, OrganizationRequest request) {
        Organization organization = organizationRepository.findById(organizationId)
                .orElseThrow(() -> new RuntimeException
                        (String.format("Organization with id %s not found", organizationId)));
        organization.setName(request.getName());
        organizationRepository.save(organization);
    }
}
