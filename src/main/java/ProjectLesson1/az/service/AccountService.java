package ProjectLesson1.az.service;

import ProjectLesson1.az.model.Account;
import ProjectLesson1.az.model.PhoneNumber;
import ProjectLesson1.az.repository.AccountRepository;
import ProjectLesson1.az.repository.PhoneNumberRepository;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
@AllArgsConstructor
public class AccountService {

    private final AccountRepository accountRepository;
    private final PhoneNumberRepository phoneNumberRepository;
//    private final CacheManager cacheManager;
    private final RedisTemplate<String,Object> redisTemplate;




    public List<Integer> getPrime(){

      List<Integer> primenums=new ArrayList<>();
        int count=0;
        while(primenums.size()<=500){
            int max=3000;
            Random random = new Random();
            int randomNumber = random.nextInt(max);
            count=0;
            for (int i=1;i<=randomNumber;i++){
                if (randomNumber%i==0){
                    count++;
                }
                if (count>2){
                    break;
                }
            }

            if (count == 2) {
                primenums.add(randomNumber);
            }
        }

      return primenums;


    }



//   @Transactional
//    public void assignphonetoacc(Long accid, List<Long> phoneids){
//        Account account=accountRepository.getReferenceById(accid);
//
//        phoneids.forEach(i->{
//            PhoneNumber phoneNumber=phoneNumberRepository.getReferenceById(i);
//            phoneNumber.setAccount(account);
//            account.getPhoneNumberList().add(phoneNumber);
//        });
//    }

    public Account getAccount(Long id) {
        Account account = (Account) redisTemplate.opsForValue().get(String.valueOf(id));
        if (account != null) {
            return account;
        }
        Account account1 = accountRepository.findById(id).orElseThrow();
        redisTemplate.opsForValue().set(String.valueOf(id), account1);
        return account1;

    }




//                   ////CACHEMANAGER///////
//    public Account getAccount(Long id) {
//        Cache cache=cacheManager.getCache("account");
//        Account accountFrCache=cache.get(id,Account.class);
//        if (accountFrCache==null){
//            Account account=accountRepository.findById(id).get();
//            cache.put(id,account);
//            return account;
//        }
//        return accountFrCache;
//
// }
//    public Account delete(Long id) {
//       Cache cache=cacheManager.getCache("account");
//        Account account=accountRepository.findById(id).orElseThrow();
//       accountRepository.delete(account);
//       cache.evict(account.getId());
//       return account;
//    }
//
//    @Transactional
//    public Account updateAcc(Account account) {
//        Cache cache=cacheManager.getCache("account");
//        Account account1=accountRepository.findById(account.getId()).orElseThrow();
//        account1.setName(account.getName());
//        account1.setBalance(account.getBalance());
//        cache.put(account1.getId(),account1);
//
//        return account1;
//    }





                           ////////////CACHEABLE//////////////


//    @Cacheable(key = "#id",cacheNames = "account")
//    public Account getAccount(Long id) {
//       return accountRepository.findById(id).get();
//    }
//    @Transactional
//    @CachePut(cacheNames = "account",key = "#account.id")
//    public Account updateAcc(Account account) {
//       Account account1 =accountRepository.findById(account.getId()).orElseThrow();
//       account1.setName(account.getName());
//       account1.setBalance(account.getBalance());
//       return account1;
//    }
//
//    @CacheEvict(cacheNames = "account",key="#id")
//    public Account delete(Long id) {
//       Account account=accountRepository.findById(id).orElseThrow();
//       accountRepository.delete(account);
//       return account;
//    }
}
