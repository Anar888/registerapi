package ProjectLesson1.az.controller;

import ProjectLesson1.az.dto.OrganizationRequest;
import ProjectLesson1.az.dto.OrganizationResponse;
import ProjectLesson1.az.service.OrganizationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/organization")
@RequiredArgsConstructor
public class OrganizationController {
    private final OrganizationService organizationService;

    @PostMapping
    public OrganizationResponse create(@RequestBody OrganizationRequest request){
        return organizationService.create(request);

    }
    @GetMapping("/{organizationId}")
    public OrganizationResponse get(@PathVariable Long organizationId) {
        return organizationService.get(organizationId);
    }
    @DeleteMapping("/{organizationId}")
    public void delete(@PathVariable Long organizationId) {
        organizationService.delete(organizationId);
    }
    @PutMapping("/{organizationId}")
    public void update(@PathVariable Long organizationId,@RequestBody OrganizationRequest request ){
        organizationService.update(organizationId,request);
    }
}
