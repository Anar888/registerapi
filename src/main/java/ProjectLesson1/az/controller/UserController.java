package ProjectLesson1.az.controller;

import ProjectLesson1.az.model.User;
import ProjectLesson1.az.repository.UserRepository;
import ProjectLesson1.az.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    private final UserRepository userRepository;

    @PostMapping("/register")
    public ResponseEntity<String> register(@RequestBody User user){
        return userService.register(user);
    }


    @GetMapping("/get")
    public ResponseEntity<String> get(){
        return ResponseEntity.ok().body("Hello");
    }



}
