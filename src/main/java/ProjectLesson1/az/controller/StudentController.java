package ProjectLesson1.az.controller;

import java.util.List;

import ProjectLesson1.az.dto.SearchCriteria;
import ProjectLesson1.az.model.Student;
import ProjectLesson1.az.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/student")
public class StudentController {

    private final StudentService studentService;

    @GetMapping("/list")
    public List<Student> getListByCriteria(@RequestBody List<SearchCriteria> dto) {
        return studentService.getAllByCriteria(dto);
    }
}

