package ProjectLesson1.az.controller;

import ProjectLesson1.az.model.Account;
import ProjectLesson1.az.service.TransferService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/transfer")
@RequiredArgsConstructor
public class TransferController {
    private final TransferService transferService;

    @GetMapping("/update")
    public Account update1(){
      return   transferService.update1();
    }
    @GetMapping("/update2")
    public Account update2(){
        return   transferService.update2();
    }
    @GetMapping
    public Account read(){
        return   transferService.read();
    }
    @GetMapping("/all")
    public List<Account> findall(){
        return   transferService.findall();
    }
}
