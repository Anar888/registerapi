package ProjectLesson1.az.controller;

import ProjectLesson1.az.dto.AppUserRequest;
import ProjectLesson1.az.dto.AppUserResponse;
import ProjectLesson1.az.service.AppUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/appuser")
@RequiredArgsConstructor
public class AppUserController {
 private  final AppUserService appUserService;
 @PostMapping
 public AppUserResponse create(@RequestBody AppUserRequest request){
     return appUserService.create(request);

 }
 @PutMapping("/{userId}")
    public void update(@PathVariable Long userId,@RequestBody AppUserRequest request){
     appUserService.update(userId,request);
 }
 @DeleteMapping("/{userId}")
 public void delete(@PathVariable Long userId) {
     appUserService.delete(userId);
 }
 @GetMapping("/{userId}")
 public AppUserResponse get(@PathVariable Long userId) {
        return appUserService.get(userId);
    }

}
