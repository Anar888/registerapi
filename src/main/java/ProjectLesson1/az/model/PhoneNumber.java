package ProjectLesson1.az.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import lombok.experimental.FieldNameConstants;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import java.io.Serializable;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "phonenumber")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
@FieldNameConstants
@ToString
public class PhoneNumber implements Serializable {
    public static final long serialVersionUID=123484382l;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String phone;
    @ManyToOne
    @ToString.Exclude
    @JsonIgnore
    Account account;
}
