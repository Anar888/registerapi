package ProjectLesson1.az.model;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import lombok.experimental.FieldNameConstants;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "account")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
@FieldNameConstants
@ToString
@Cacheable(value = "account", key = "#id")
public class Account implements Serializable {
public static final long serialVersionUID=123484382l;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String name;
    Double balance;
@OneToMany(mappedBy = "account",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
List<PhoneNumber> phoneNumberList=new ArrayList<>();
    @Version
    Long version;
}
