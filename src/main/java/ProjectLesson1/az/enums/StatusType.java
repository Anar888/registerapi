package ProjectLesson1.az.enums;

public enum StatusType {
    ACTIVE,
    TODO,
    DONE
}
